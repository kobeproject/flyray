package me.flyray.rbac.dao;

import me.flyray.rbac.entity.SysOrgEntity;

/** 
* @author: bolei
* @date：2017年4月4日 下午12:03:44 
* @description：组织机构
*/

public interface SysOrgDao extends BaseDao<SysOrgEntity> {

	
}
