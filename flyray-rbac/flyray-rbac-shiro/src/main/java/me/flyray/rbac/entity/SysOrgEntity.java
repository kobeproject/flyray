package me.flyray.rbac.entity;

import java.io.Serializable;
import java.util.Date;

/** 
* @author: bolei
* @date：2017年4月4日 上午10:44:39 
* @description：机构组织
*/

public class SysOrgEntity implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Long orgId;
	
	/**
	 * 机构部门编号
	 */
	private String orgNo;
	
	/**
	 * 机构部门名称
	 */
	private String orgName;
	
	/**
	 * 所属机构部门
	 */
	private Long parentId;
	
	/**
	 * 创建人user_id
	 */
	private Long createBy;
	
	/**
	 * 创建时间
	 */
	private Date createTime;
	
	/**
	 * 最后修改人user_id
	 */
	private Long lastUpdateBy;
	
	/**
	 * 最后修改时间
	 */
	private Date lastUpdateTime;
	
	/**
	 * 机构部门排序号
	 */
	private String orgLevel;

	public Long getOrgId() {
		return orgId;
	}

	public void setOrgId(Long orgId) {
		this.orgId = orgId;
	}

	public String getOrgNo() {
		return orgNo;
	}

	public void setOrgNo(String orgNo) {
		this.orgNo = orgNo;
	}

	public String getOrgName() {
		return orgName;
	}

	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

	public Long getParentId() {
		return parentId;
	}

	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}

	public Long getCreateBy() {
		return createBy;
	}

	public void setCreateBy(Long createBy) {
		this.createBy = createBy;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Long getLastUpdateBy() {
		return lastUpdateBy;
	}

	public void setLastUpdateBy(Long lastUpdateBy) {
		this.lastUpdateBy = lastUpdateBy;
	}

	public Date getLastUpdateTime() {
		return lastUpdateTime;
	}

	public void setLastUpdateTime(Date lastUpdateTime) {
		this.lastUpdateTime = lastUpdateTime;
	}

	public String getOrgLevel() {
		return orgLevel;
	}

	public void setOrgLevel(String orgLevel) {
		this.orgLevel = orgLevel;
	}
	
}
