package me.flyray.erp.dao;

import java.util.List;

import me.flyray.erp.model.User;

public interface UserDao {
	
	public List<User> queryUser();
}
