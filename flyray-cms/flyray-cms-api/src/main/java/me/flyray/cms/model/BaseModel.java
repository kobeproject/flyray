package me.flyray.cms.model;

import java.io.Serializable;

@SuppressWarnings("serial")
public abstract class BaseModel implements Serializable {
	
private String merchantNo;
	
	private String orgNo;

	public String getMerchantNo() {
		return merchantNo;
	}

	public void setMerchantNo(String merchantNo) {
		this.merchantNo = merchantNo;
	}

	public String getOrgNo() {
		return orgNo;
	}

	public void setOrgNo(String orgNo) {
		this.orgNo = orgNo;
	}
	
}
